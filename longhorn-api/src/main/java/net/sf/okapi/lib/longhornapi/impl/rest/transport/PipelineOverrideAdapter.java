package net.sf.okapi.lib.longhornapi.impl.rest.transport;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class PipelineOverrideAdapter extends XmlAdapter<PipelineOverrideAdapter.AdaptedOverride, PipelineOverride> {

	@Override
	public PipelineOverride unmarshal(AdaptedOverride v) throws Exception {
		if ("filterConfig".equals(v.type)) {
			FilterConfigOverride override = new FilterConfigOverride();
			override.setType("filterConfig");
			override.setFileExtension(v.fileExtension);
			override.setFilterName(v.filterName);
			override.setFilterParams(v.filterParams);
			return override;
		} else {
			StepConfigOverride override = new StepConfigOverride();
			override.setType("stepConfig");
			override.setStepClassName(v.stepClassName);
			override.setStepParams(v.stepParams);
			return override;
		}
	}

	@Override
	public AdaptedOverride marshal(PipelineOverride override) throws Exception {
		AdaptedOverride adapted = new AdaptedOverride();
		adapted.type = override.getType();
		if (FilterConfigOverride.class.equals(override.getClass())) {
			FilterConfigOverride fco = (FilterConfigOverride) override;
			adapted.fileExtension = fco.getFileExtension();
			adapted.filterName = fco.getFilterName();
			adapted.filterParams = fco.getFilterParams();
		} else {
			StepConfigOverride sco = (StepConfigOverride) override;
			adapted.stepClassName = sco.getStepClassName();
			adapted.stepParams = sco.getStepParams();
		}
		return adapted;
	}

	static class AdaptedOverride {
		@XmlAttribute
		String type;
		@XmlElement(required = false)
		String fileExtension;
		@XmlElement(required = false)
		String filterName;
		@XmlElement(required = false)
		String filterParams;
		@XmlElement(required = false)
		String stepClassName;
		@XmlElement(required = false)
		String stepParams;
	}
}
